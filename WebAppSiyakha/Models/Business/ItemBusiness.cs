﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAppSiyakha.Models.Business
{
    public class ItemBusiness : Controller
    {
        private ApplicationDbContext applicationDb = new ApplicationDbContext();

        public IEnumerable<Item> GetItems()
        {
            return applicationDb.Items.ToList(); //dataContext.Items.Include(i => i.Department).ToList();
        }
        public void AddItem(Item item)
        {
            try
            {
                applicationDb.Items.Add(item);
                applicationDb.SaveChanges();
               
            }
            catch (Exception)
            {
            
            }
        }

        public bool UpdateItem(Item item)
        {
            try
            {
                applicationDb.Entry(item).State = EntityState.Modified;
                applicationDb.SaveChanges();
                return true;
            }
            catch (Exception)
            { return false; }
        }
        public bool RemoveItem(Item item)
        {
            try
            {
                applicationDb.Items.Remove(item);
                applicationDb.SaveChanges();
                return true;
            }
            catch (Exception)
            { return false; }
        }
        public Item GetItem(int item_id)
        {
            return applicationDb.Items.Where(l=>l.ItemCode== item_id).FirstOrDefault();
        }


        public string imageUrI(HttpPostedFileBase file, Item item)
        {

            if (item != null)
            {
                string ext = Path.GetExtension(file.FileName);
                if (ext != ".png" && ext != ".PNG" && ext != ".jpg" && ext != ".JPG" && ext != ".jpeg" && ext != ".JPEG")
                {

                }
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Content/Images/"), Guid.NewGuid().ToString() + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    return path.Substring(path.LastIndexOf("\\") + 1);
                }
                catch (Exception e)
                {
                    var err = e.Message;
                }
            }
            return "Error Check Image";
        }
    }
}