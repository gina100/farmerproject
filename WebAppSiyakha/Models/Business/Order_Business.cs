﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppSiyakha.Models.Business
{
    public class Order_Business
    {
        private ApplicationDbContext applicationDb = new ApplicationDbContext();

        public List<Order> cust_all()
        {
            return applicationDb.Orders.ToList();
        }

        public List<Order> cust_find_by_status(string status)
        {
            return applicationDb.Orders.Where( x => x.status.ToLower() == status.ToLower()).ToList();
        }

        public Order cust_find_by_id(int id)
        {
            return applicationDb.Orders.Find(id);
        }
        public List<OrderItem> cust_Order_items(int id)
        {
            return cust_find_by_id(id).orderItems.ToList();
        }

        public List<OrderTracking> get_tracking_report(int? id)
        {
            return applicationDb.Order_Trackings.Where(x => x.tracking_ID == id).ToList();
        }
        public void mark_as_packed(int id)
        {
            var order = cust_find_by_id(id);
            order.packed = true;
            if (applicationDb.OrderAddresses.Where(p => p.OrderNo == id) != null)
            {
                order.status = "With courier";

                applicationDb.Order_Trackings.Add(new OrderTracking()
                {
                    orderNo = order.OrderNo,
                    date = DateTime.Now,
                    status = "Order Packed, Now with our courier",
                    Recipient = ""
                });
            }

            applicationDb.SaveChanges();
        }
        public void schedule_delivery(int Order_Id, DateTime date)
        {
            //Order_Id
            var order = cust_find_by_id(Order_Id);
            order.status = "Scheduled for delivery";
            //order tracking
            applicationDb.Order_Trackings.Add(new OrderTracking()
            {
                orderNo = order.OrderNo,
                date = DateTime.Now,
                status = "Scheduled for delivery on " + date.ToLongDateString(),
                Recipient = ""
            });
            applicationDb.SaveChanges();
        }
    }
}