﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebAppSiyakha.Models.Business
{
    public class CartBusiness
    {
        private ApplicationDbContext applicationDb = new ApplicationDbContext();

        public static string shoppingCartID { get; set; }
        public ProductOrder ProductOrder { get; private set; }
        public const string CartSessionKey = "CartId";
        public void AddItemToCart(int id, string username)
        {
            shoppingCartID = GetCartID();
            ProductOrder = new ProductOrder();
            var item = applicationDb.Items.Where(l=>l.ItemCode==id).FirstOrDefault();
            if (item != null)
            {

                //FoodOrder
                var foodItem =applicationDb.ProductOrder.Where(x => x.cart_id == shoppingCartID && x.item_id == item.ItemCode).FirstOrDefault();
                var cartItem =
                    applicationDb.Cart_Items.Where(x => x.cartId == shoppingCartID && x.ItemdId == item.ItemCode).FirstOrDefault();
                if (cartItem == null)
                {
                    var cart = applicationDb.Carts.Where(l=>l.CartID==shoppingCartID).FirstOrDefault();
                    if (cart == null)
                    {
                        applicationDb.Carts.Add(new Cart()
                        {

                            CartID = shoppingCartID,
                            date_created = DateTime.Now

                        });
                        applicationDb.SaveChanges();
                    }

                    applicationDb.Cart_Items.Add(new CartItem()
                    {
                        Id = Guid.NewGuid().ToString(),
                        cartId = shoppingCartID,
                        ItemdId = item.ItemCode,
                        quantity = 1,
                        price = item.Price,
                        UserEmail = username
                    }
                        );
                    applicationDb.ProductOrder.Add( new ProductOrder()
                    {
                        cart_item_id = Guid.NewGuid().ToString(),
                        cart_id = shoppingCartID,
                        item_id = item.ItemCode,
                        quantity = 1,
                        price = item.Price,
                        ItemName = item.Name,
                        UserEmail = username,
                        Picture = item.Picture,
                        OrderDate = DateTime.Now.Date.ToString(),
                        OrderStatus = "Not Cheked Out"
                    });

                }
                else
                {
                    cartItem.quantity++;
                    foodItem.quantity++;
                }
                applicationDb.SaveChanges();
            }
        }
        public void UpdateQuantity(int id, int qty)
        {
            var qtyUpdate = applicationDb.Items.Where(l => l.ItemCode == id).FirstOrDefault();
            qtyUpdate.QuantityInStock = qtyUpdate.QuantityInStock - qty;
            applicationDb.Entry(qtyUpdate).State = EntityState.Modified;
            applicationDb.SaveChanges();
        }
        public void increaseItemQuantity(string id)
        {
            shoppingCartID = GetCartID();

            var item = applicationDb.Cart_Items.Where(l=>l.Id==id).FirstOrDefault();
            if (item != null)
            {
                var cartItem = applicationDb.Cart_Items.Where( x => x.cartId == shoppingCartID && x.ItemdId == item.ItemdId).FirstOrDefault();
                //var OrderItem =
                //   _productOrderRepository.Find(predicate: x => x.cart_id == shoppingCartID && x.item_id == item.ItemdId).FirstOrDefault();
                if (cartItem != null)
                {
                    cartItem.quantity += 1;
                    applicationDb.Entry(cartItem).State = EntityState.Modified;
                    applicationDb.SaveChanges();
                    // _productOrderRepository.Delete(model: OrderItem);
                }
                //dataContext.SaveChanges();
            }
        }
        public void decreaseItemQuantity(string id)
        {
            shoppingCartID = GetCartID();

            var item = applicationDb.Cart_Items.Where(l => l.Id == id).FirstOrDefault();
            if (item != null)
            {
                var cartItem = applicationDb.Cart_Items.Where(x => x.cartId == shoppingCartID && x.ItemdId == item.ItemdId).FirstOrDefault();

                //var OrderItem =
                //   _productOrderRepository.Find(predicate: x => x.cart_id == shoppingCartID && x.item_id == item.ItemdId).FirstOrDefault();
                if (cartItem != null)
                {
                    if (cartItem.quantity == 1)
                        applicationDb.Cart_Items.Remove(cartItem);
                    else
                    {
                        cartItem.quantity -= 1;
                        applicationDb.Entry(cartItem).State = EntityState.Modified;
                    }

                    applicationDb.SaveChanges();

                    // _productOrderRepository.Delete(model: OrderItem);
                }
                //dataContext.SaveChanges();
            }
        }

        public void RemoveItemFromCart(string id)
        {
            shoppingCartID = GetCartID();

            var item = applicationDb.Cart_Items.Where(l => l.Id == id).FirstOrDefault();

            if (item != null)
            {
                var cartItem = applicationDb.Cart_Items.Where(x => x.cartId == shoppingCartID && x.ItemdId == item.ItemdId).FirstOrDefault();

                var OrderItem =
                   applicationDb.ProductOrder.Where(x => x.cart_id == shoppingCartID && x.item_id == item.ItemdId).FirstOrDefault();
                if (cartItem != null)
                {
                    applicationDb.Cart_Items.Remove(cartItem);
                    applicationDb.ProductOrder.Remove( OrderItem);
                }

                applicationDb.SaveChanges();
            }
        }
        public List<CartItem> GetCartItems()
        {
            shoppingCartID = GetCartID();
            return applicationDb.Cart_Items.Where(x => x.cartId == shoppingCartID).ToList();
        }
        public void UpdateCart(string id, int qty)
        {
            var item = applicationDb.Cart_Items.Where(l => l.Id == id).FirstOrDefault();
            if (qty < 0)
                item.quantity = qty / -1;
            else if (qty == 0)
                RemoveItemFromCart(item.cartId);
            else if (item.Item.QuantityInStock < qty)
                item.quantity = item.Item.QuantityInStock;
            else
                item.quantity = qty;
            //  dataContext.SaveChanges();
        }
        public decimal GetCartTotal(string id)
        {
            double amount = 0;
            foreach (var item in applicationDb.Cart_Items.Where(x => x.cartId == id))
            {
                amount += (item.price * item.quantity);
            }
            return (decimal)amount;
        }
        public void EmptyCart()
        {
            shoppingCartID = GetCartID();

            foreach (var item in applicationDb.Cart_Items.Where(x => x.cartId == shoppingCartID)) 
            {
                applicationDb.Cart_Items.Remove(item);
            }
            applicationDb.SaveChanges();
            try
            {
                var cart = applicationDb.Carts.Where(l => l.CartID == shoppingCartID).FirstOrDefault();

                applicationDb.Carts.Remove(cart);
                applicationDb.SaveChanges();
            }
            catch (Exception x)
            {
                var m = x.Message;
            }
        }
        public string GetCartID()
        {
            if (System.Web.HttpContext.Current.Session[name: CartSessionKey] == null)
            {
                if (!String.IsNullOrWhiteSpace(value: System.Web.HttpContext.Current.User.Identity.Name))
                {
                    System.Web.HttpContext.Current.Session[name: CartSessionKey] = System.Web.HttpContext.Current.User.Identity.Name;
                }
                else
                {
                    Guid temp = Guid.NewGuid();
                    System.Web.HttpContext.Current.Session[name: CartSessionKey] = temp.ToString();
                }
            }

            return System.Web.HttpContext.Current.Session[name: CartSessionKey].ToString();
        }

        public void AddPayment(Order order)
        {
            var payment = new Payment()
            {
                AmountPaid = (double)order.TotalPrice,
                custId = order.Email,
                Date = DateTime.Now,
                PaymentFor = "Online Order No" + order.OrderNo.ToString(),
                PaymentMethod = "Payfast"
            };
            applicationDb.Payments.Add(payment);
            applicationDb.SaveChanges();
        }
    }
}