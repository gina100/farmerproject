﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace WebAppSiyakha.Models.Business
{
    public class CategoryBusiness
    {
        private ApplicationDbContext applicationDb = new ApplicationDbContext();

        public List<Category> GetCategories()
        {
            return applicationDb.Categories.ToList();
        }
        public bool AddCategory(Category category)
        {
            try
            {
                
                applicationDb.Categories.Add(category);
                applicationDb.SaveChanges();
                return true;
            }
            catch (Exception)
            { return false; }
        }
        public bool UpdateCategory(Category category)
        {
            try
            {
                applicationDb.Entry(category).State = EntityState.Modified;
                applicationDb.SaveChanges();
                return true;
            }
            catch (Exception)
            { return false; }
        }
        public bool RemoveCategory(Category category)
        {
            try
            {
                applicationDb.Categories.Remove(category);
                applicationDb.SaveChanges();
                return true;
            }
            catch (Exception)
            { return false; }
        }
        public Category GetCategory(int category_id)
        {
            return applicationDb.Categories.Where(l=> l.Category_ID==category_id).FirstOrDefault(); 
        }
    }
}