﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebAppSiyakha.Models.Business
{
    public class DepartmentBusiness
    {
        private ApplicationDbContext applicationDb = new ApplicationDbContext();

        public List<Department> GetDepartments()
        {
            return applicationDb.Departments.ToList(); //dataContext.Departments.ToList();
        }
        public bool AddDepartment(Department department)
        {
            try
            {
                applicationDb.Departments.Add(department);
                applicationDb.SaveChanges();
                return true;
            }
            catch (Exception)
            { return false; }
        }
        public bool UpdateDepartment(Department department)
        {
            try
            {
                applicationDb.Entry(department).State = EntityState.Modified;
                applicationDb.SaveChanges();
                return true;
            }
            catch (Exception)
            { return false; }
        }
        public bool RemoveDepartment(Department department)
        {
            try
            {
                applicationDb.Departments.Remove(department);
                applicationDb.SaveChanges();
                return true;
            }
            catch (Exception)
            { return false; }
        }
        public Department GetDepartment(int department_id)
        {
            return applicationDb.Departments.Where(l => l.Department_ID == department_id).FirstOrDefault();
        }
    }
}