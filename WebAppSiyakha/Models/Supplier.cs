﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebAppSiyakha.Models;

namespace WebAppSiyakha.Models
{
   public class Supplier
    {
        [Key]
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<Item> Items { get; set; }
    }
}
