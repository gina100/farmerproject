namespace WebAppSiyakha.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hjhj : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Manuals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PdfName = c.String(nullable: false),
                        pdf = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Manuals");
        }
    }
}
